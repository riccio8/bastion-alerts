﻿namespace Bastion.Alerts
{
    public class AlertNameConstants
    {
    
        public const string BYINST_DEPOSIT = "BYINST_DEPOSIT";
        public const string BYCARD_DEPOSIT = "BYCARD_DEPOSIT";
        public const string OVERDRAFT_PROTECT = "OVERDRAFT_PROTECT";
        public const string ATM_DEPOSIT = "ATM_DEPOSIT";
        public const string OVERDRAFT = "OVERDRAFT";
        //public const string INCOMING_WIRE = "INCOMING_WIRE";
        public const string INC_WIRE_EXCEED = "INC_WIRE_EXCEED";
        
        public const string BILL_EXCEED = "BILL_EXCEED";
        public const string OUT_WIRE_EXCEED = "OUT_WIRE_EXCEED";
        public const string OUT_WIRE_EU = "OUT_WIRE_EU";
        public const string OUT_WIRE_NONEU = "OUT_WIRE_NONEU";

        public const string TRANSFER = "TRANSFER";

        

        public const string CARD_TRAN_EXCEED = "CARD_TRAN_EXCEED";
        public const string ATM_WITHDR_EXCEED = "ATM_WITHDR_EXCEED";
        public const string BALANCE_BELOW_XXX = "BALANCE_BELOW_XXX";
        public const string PAYEE_ADDED = "PAYEE_ADDED";
        public const string DEPOSIT_GREATER_XXX = "DEPOSIT_GREATER_XXX";
        public const string USERMONEY_SENT = "USERMONEY_SENT";
        public const string SYS_WITHDRAW = "SYS_WITHDRAW";
        public const string SYS_DEPOSIT_ARRIVED = "SYS_DEPOSIT_ARRIVED";
        public const string USERMONEY_SEND_ERR = "USERMONEY_SEND_ERR";
        public const string USERMONEY_ARRIVED = "USERMONEY_ARRIVED";
        public const string BALANCE_ABOVE_XXX = "BALANCE_ABOVE_XXX";

        //public const string BalanceNegative = "BALANCE_NEGATIVE";
        //public const string DepositGreaterXXX = "DEPOSIT_GREATER_XXX";
        //public const string BalanceAboveXXX = "BALANCE_ABOVE_XXX";
        //public const string BalanceBelowXXX = "BALANCE_BELOW_XXX";
        //public const string ATMDeposit = "ATM_DEPOSIT";
        //public const string Ovedraft = "OVERDRAFT";
        //public const string IncomingWire = "INCOMING_WIRE";
        //public const string BillExceed = "BILL_EXCEED";
        //public const string OutgoingWire = "OUTGOING_WIRE";
        //public const string CardTranExceed = "CARD_TRAN_EXCEED";
        //public const string ATMWithdrExceed = "ATM_WITHDR_EXCEED";
        //public const string PayeeAdded = "PAYEE_ADDED";
        //public const string UsermoneySent = "USERMONEY_SENT";
        //public const string SysWithdraw = "SYS_WITHDRAW";
        //public const string SysDepositArrived = "SYS_DEPOSIT_ARRIVED";
        //public const string UsermoneyArrived = "USERMONEY_ARRIVED";
        //public const string UsermoneySendErr = "USERMONEY_SEND_ERR";


    }
}
