﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Bastion.Alerts.Models
{
    public partial class AlertContext : DbContext
    {
        public AlertContext()
        {
        }

        public AlertContext(DbContextOptions<AlertContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alert> Alert { get; set; }
        public virtual DbSet<AlertAccount> AlertAccount { get; set; }
        public virtual DbSet<AlertUser> AlertUser { get; set; }
        public virtual DbSet<Extra> Extra { get; set; }
        public virtual DbSet<Frequency> Frequency { get; set; }
        public virtual DbSet<Group> Group { get; set; }

        // Unable to generate entity type for table 'alert.template'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Alert>(entity =>
            {
                entity.ToTable("alert", "alert");

                entity.Property(e => e.AlertId)
                    .HasColumnName("alert_id")
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.DefaultSendEmail)
                    .IsRequired()
                    .HasColumnName("default_send_email")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.DefaultSendPush)
                    .IsRequired()
                    .HasColumnName("default_send_push")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.DefaultSendSms)
                    .IsRequired()
                    .HasColumnName("default_send_sms")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.EmailText)
                    .HasColumnName("email_text")
                    .HasMaxLength(500);

                entity.Property(e => e.ExtraId)
                    .IsRequired()
                    .HasColumnName("extra_id")
                    .HasColumnType("character varying")
                    .HasDefaultValueSql("'NONE'::character varying");

                entity.Property(e => e.FrequencyId)
                    .IsRequired()
                    .HasColumnName("frequency_id")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'ASAP'::character varying");

                entity.Property(e => e.GroupId)
                    .IsRequired()
                    .HasColumnName("group_id")
                    .HasMaxLength(15)
                    .HasDefaultValueSql("'BANK_BALANCE'::character varying");

                entity.Property(e => e.HasValue).HasColumnName("has_value");

                entity.Property(e => e.MessageId)
                    .HasColumnName("message_id")
                    .HasMaxLength(36);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasMaxLength(20)
                    .ForNpgsqlHasComment("if this alert can only be visible to specific ASP.NET ROLE");

                entity.Property(e => e.ShowDimm)
                    .IsRequired()
                    .HasColumnName("show_dimm")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.ShowMigom)
                    .IsRequired()
                    .HasColumnName("show_migom")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.SmsText)
                    .HasColumnName("sms_text")
                    .HasMaxLength(255);

                entity.Property(e => e.SortOrder).HasColumnName("sort_order");

                entity.HasOne(d => d.Extra)
                    .WithMany(p => p.Alert)
                    .HasForeignKey(d => d.ExtraId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("alert_extra_id_fkey");

                entity.HasOne(d => d.Frequency)
                    .WithMany(p => p.Alert)
                    .HasForeignKey(d => d.FrequencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("alert_frequency_id_fkey");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Alert)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("alert_alert_group_id_fkey");
            });

            modelBuilder.Entity<AlertAccount>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.AlertId })
                    .HasName("user_alert_pkey");

                entity.ToTable("alert_account", "alert");

                entity.Property(e => e.AccountId)
                    .HasColumnName("account_id")
                    .HasMaxLength(36);

                entity.Property(e => e.AlertId)
                    .HasColumnName("alert_id")
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .ForNpgsqlHasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Push).HasColumnName("push");

                entity.Property(e => e.Sms)
                    .HasColumnName("sms")
                    .ForNpgsqlHasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("numeric");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.AlertAccount)
                    .HasForeignKey(d => d.AlertId)
                    .HasConstraintName("user_alert_alert_id_fkey");
            });

            modelBuilder.Entity<AlertUser>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.AlertId })
                    .HasName("alert_account_copy_pkey");

                entity.ToTable("alert_user", "alert");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.AlertId)
                    .HasColumnName("alert_id")
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .ForNpgsqlHasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Push).HasColumnName("push");

                entity.Property(e => e.Sms)
                    .HasColumnName("sms")
                    .ForNpgsqlHasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.AlertUser)
                    .HasForeignKey(d => d.AlertId)
                    .HasConstraintName("alert_account_copy_alert_id_fkey");
            });

            modelBuilder.Entity<Extra>(entity =>
            {
                entity.ToTable("extra", "alert");

                entity.Property(e => e.ExtraId)
                    .HasColumnName("extra_id")
                    .HasColumnType("character varying")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Frequency>(entity =>
            {
                entity.ToTable("frequency", "alert");

                entity.Property(e => e.FrequencyId)
                    .HasColumnName("frequency_id")
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("group", "alert");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasMaxLength(15)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountTypeId)
                    .IsRequired()
                    .HasColumnName("account_type_id")
                    .HasMaxLength(25);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.SortOrder).HasColumnName("sort_order");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'ACCOUNT'::character varying");
            });
        }
    }
}
