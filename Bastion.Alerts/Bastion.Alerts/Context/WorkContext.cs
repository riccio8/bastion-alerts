﻿namespace Bastion.Alerts.Context
{
    using System;
    using Bastion.Alerts.Models;
    using Bastion.Kernel;
    using Microsoft.EntityFrameworkCore;

    public class WorkContext : AlertContext
    {
        public WorkContext()
        {
        }

        public WorkContext(DbContextOptions<AlertContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var connectionString = CfgKernel.Configuration["DbConnection"];
                if (connectionString == null)
                {
                    throw new Exception("Missing config section DbConnection");
                }

                optionsBuilder.UseNpgsql(connectionString);
            }
        }
    }
}
