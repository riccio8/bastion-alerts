﻿using System;
using System.Collections.Generic;

namespace Bastion.Alerts.Models
{
    public partial class AlertUser
    {
        public string UserId { get; set; }
        public string AlertId { get; set; }
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public bool Push { get; set; }
        public string Value { get; set; }

        public virtual Alert Alert { get; set; }
    }
}
