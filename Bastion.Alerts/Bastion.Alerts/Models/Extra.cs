﻿using System;
using System.Collections.Generic;

namespace Bastion.Alerts.Models
{
    public partial class Extra
    {
        public Extra()
        {
            Alert = new HashSet<Alert>();
        }

        public string ExtraId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Alert> Alert { get; set; }
    }
}
