﻿using System;
using System.Collections.Generic;

namespace Bastion.Alerts.Models
{
    public partial class Frequency
    {
        public Frequency()
        {
            Alert = new HashSet<Alert>();
        }

        public string FrequencyId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Alert> Alert { get; set; }
    }
}
