﻿using System;
using System.Collections.Generic;

namespace Bastion.Alerts.Models
{
    public partial class Group
    {
        public Group()
        {
            Alert = new HashSet<Alert>();
        }

        public string GroupId { get; set; }
        public string AccountTypeId { get; set; }
        public string Name { get; set; }
        public short? SortOrder { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Alert> Alert { get; set; }
    }
}
