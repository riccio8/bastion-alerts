﻿namespace BaltikaLight.Services
{

    using Bastion.Accounts.Models;
    using Bastion.PubSub;
    using Bastion.Senders.EmailSender.Abstractions;
    using Bastion.Senders.SmsSender.Abstractions;


    using Microsoft.AspNetCore.Identity;

    using System;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Alert service.
    /// </summary>
    public class SystemAlertService
    {
        private readonly ISmsSender smsSender;
        private readonly IEmailSender emailSender;
        private readonly IMessagesService messagesService;
        private readonly IOnBoardRepository onBoardRepository;
        private readonly UserManager<BastionUser> userManager;

        /// <summary>
        /// Initializes a new instance of <see cref="SystemAlertService"/>.
        /// </summary>
        public SystemAlertService(
            ISmsSender smsSender,
            IEmailSender emailSender,
            IMessagesService messagesService,
            IOnBoardRepository onBoardRepository,
            UserManager<BastionUser> userManager)
        {
            this.smsSender = smsSender;
            this.repository = repository;
            this.emailSender = emailSender;
            this.userManager = userManager;
            this.messagesService = messagesService;
            this.onBoardRepository = onBoardRepository;
        }

        //private async Task<Alert> GetAlertAsync(BastionUser user, string alertTypeId)
        //{
        //    if (user.EmailConfirmed || user.PhoneNumberConfirmed)
        //    {
        //        var alertType = await repository.GetFirstEntityAsync<AlertType>(x
        //            => x.AlertTypeId == alertTypeId);

        //        if (!string.IsNullOrEmpty(alertType?.MessageId))
        //        {
        //            var alert = await repository.GetEntity<Alert>(x
        //                => x.AlertTypeId == alertType.AlertTypeId && x.UserId == user.Id);
        //            if (alert != null)
        //            {
        //                alert.AlertType = alertType;

        //                return alert;
        //            }
        //        }
        //    }

        //    return default;
        //}

        private async Task<AlertUser> GetAlertAsync(BastionUser user, string alertId)
        {
            var alertUser = await repository.GetEntity<AlertUser>(x => x.AlertId == alertId, includes: nameof(Alert));
            if (alertUser != null)
                return alertUser;
            else
                return default;
        }

        private async Task SendAlertAsync<T>(BastionUser user, AlertUser alertUser, string languageId, T data) where T : class
        {
            if (alertUser.Email && user.EmailConfirmed)
            {
                await emailSender.SendEmailAsync(user.Email, messagesService, alertUser.Alert.MessageId, languageId, data);
            }
            if (alertUser.Sms && user.PhoneNumberConfirmed)
            {
                await smsSender.SendSmsAsync(user.PhoneNumber, messagesService, alertUser.Alert.MessageId, languageId, data);
            }
        }

        //void Replace(StringBuilder sb, string template)
        //{
        //    // {UserName}, совершён вход в {SiteName} {Now}. Если вход произведён не вами, обратитесь в контактный центр.
        //    sb.Append(template
        //                      .Replace("{Now}", DateTime.Now.ToString("HH:mm:ss"))
        //                      .Replace("{Time}", DateTime.Now.ToString("HH:mm:ss"))
        //                      .Replace("{SiteName}", DateTime.Now.ToString("HH:mm:ss"))
        //                      .Replace("{UserName}", DateTime.Now.ToString("HH:mm:ss"))
        //                      );
        //}

        /// <summary>
        /// Send alert when login success.
        /// </summary>
        /// <param name="user">User.</param>
        /// <returns>The task.</returns>
        public async Task SendLoginSuccessAsync(BastionUser user)
        {
            var alert = await GetAlertAsync(user, "LOGIN");
            if (alert != null)
            {
                var profile = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == user.Id);
                await SendAlertAsync(user, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Now = DateTime.UtcNow,
                    UserName = profile.ToName()
                });
            }
        }

        /// <summary>
        /// Send a login alert from a new device.
        /// </summary>
        /// <param name="user">User.</param>
        /// <param name="device">Device.</param>
        /// <returns>The task.</returns>
        //public async Task SendLoginNewDeviceAsync(BastionUser user, BastionUserDevice device)
        //{
        //    var alert = await GetAlertAsync(user, AlertConstants.LoginNewDevice);
        //    if (alert != null)
        //    {
        //        var profile = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == user.Id);
        //        await SendAlertAsync(user, alert, profile.GetLanguageIdOrDefault(), new
        //        {
        //            Now = DateTime.UtcNow,
        //            UserName = profile.ToName(),

        //            device.City,
        //            device.Country,
        //            device.Browser,
        //            IpAddress = device.Ip,
        //        });
        //    }
        //}

        // Send alert when deposit success

        /// <summary>
        /// Send alert when request money.
        /// </summary>
        /// <param name="userIdTo">User id to.</param>
        /// <param name="userIdFrom">User id from.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="currencyId">Currency id.</param>
        /// <param name="description">Description.</param>
        /// <param name="notificationId">Ntification id.</param>
        /// <returns>The task.</returns>
        public async Task SendRequestMoneyAsync(string userIdTo, string userIdFrom, decimal amount, string currencyId, string description, string notificationId)
        {
            var userTo = await userManager.FindByIdAsync(userIdTo);
            var userFrom = await userManager.FindByIdAsync(userIdFrom);

            if (userTo != null && userTo.EmailConfirmed || userTo.PhoneNumberConfirmed)
            {
                //        var alertType = await repository.GetFirstEntityAsync<AlertType>(x
                //            => x.AlertTypeId == AlertConstants.MoneyRequest);

                // TODO: запрос денег должен быть для Пользователя? или сразу на конкретный счёт пользователя?
                // пока не понятно

                //var alertUser = await repository.GetFirstEntityAsync<AlertUser>(x
                //    => x.AlertId == AlertConstants.MoneyRequest);

                //        if (alertType != null && !string.IsNullOrEmpty(alertType.MessageId))
                //        {
                //            var alert = await repository.GetEntity<Alert>(x
                //                => x.AlertTypeId == alertType.AlertTypeId && x.UserId == userTo.Id);

                //            if (alert != null)
                //            {
                //                var profileTo = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == userTo.Id);
                //                var profileFrom = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == userFrom.Id);

                //                if (alert.Email && userTo.EmailConfirmed)
                //                {
                //                    await emailSender.SendEmailAsync(userTo.Email, messagesService, alertType.MessageId, profileTo.LanguageId, new
                //                    {
                //                        Amount = amount,
                //                        Currency = currencyId,
                //                        Now = DateTime.UtcNow,
                //                        Description = description,
                //                        UserName = profileTo.ToName(),
                //                        NotificationId = notificationId,
                //                        UserNameFrom = profileFrom.ToName(),
                //                        UserEmailFrom = profileFrom.Email,
                //                    });
                //                }
                //                if (alert.Sms && userTo.PhoneNumberConfirmed)
                //                {
                //                    await smsSender.SendSmsAsync(userTo.PhoneNumber, messagesService, alertType.MessageId, profileTo.LanguageId, new
                //                    {
                //                        Amount = amount,
                //                        Currency = currencyId,
                //                        Now = DateTime.UtcNow,
                //                        UserNameFrom = profileFrom.ToName(),
                //                        UserEmailFrom = profileFrom.Email
                //                    });
                //                }
                //            }
                //        }
            }
        }

        #region -- подтверждение почты, смена паролей и прочие системные сообщения --
        /// <summary>
        /// Send email welcome.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailWelcomeAsync(BastionUser user)
        {
            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            if (profile.IsCorporateAccount())
            {
                var corporateDocuments = await onBoardRepository.GetDocumentTypesAsync();

                await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.WelcomeCorporate, profile.GetLanguageIdOrDefault(), new
                {
                    UserName = profile.ToName(),
                    Documents = "<ul>" + string.Join(string.Empty, corporateDocuments.Select(x => $"<li><a href=\"{x.Url}\">{x.Title}</a></li>")) + "</ul>",
                });
            }
            else
            {
                await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.Welcome, profile.GetLanguageIdOrDefault(), new
                {
                    UserName = profile.ToName()
                });
            }
        }

        /// <summary>
        /// Send email confirmation.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailConfirmationAsync(BastionUser user)
        {
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            var codeBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(code));

            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.EmailConfirm, profile.GetLanguageIdOrDefault(), new
            {
                UserId = user.Id,
                Code = codeBase64,
                UserName = profile.ToName()
            });
        }

        /// <summary>
        /// Send email password request.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailPasswordRequestAsync(BastionUser user)
        {
            var code = await userManager.GeneratePasswordResetTokenAsync(user);
            var codeBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(code));

            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.PasswordReset, profile.GetLanguageIdOrDefault(), new
            {
                UserId = user.Id,
                Code = codeBase64,
                UserName = profile.ToName()
            });
        }

        /// <summary>
        /// Send email password reset success.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailPasswordResetSuccessAsync(BastionUser user)
        {
            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.PasswordResetSuccess, profile.GetLanguageIdOrDefault(), new
            {
                UserName = profile.ToName()
            });
        }

        /// <summary>
        /// Send email two factor code.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="code">Two factor code</param>
        /// <returns>The task.</returns>
        public async Task SendEmailTwoFactorCodeAsync(BastionUser user, string code)
        {
            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.TwoFactorCode, profile.GetLanguageIdOrDefault(), new
            {
                UserName = profile.ToName(),
                Code = code
            });
        }

        /// <summary>
        /// Send sms two factor code.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="code">Two factor code</param>
        /// <returns>The task.</returns>
        public async Task SendSmsTwoFactorCodeAsync(BastionUser user, string code)
        {
            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            await smsSender.SendSmsAsync(user.PhoneNumber, messagesService, MessageConstants.TwoFactorCode, profile.GetLanguageIdOrDefault(), new
            {
                Code = code
            });
        }

        /// <summary>
        /// Send sms change phone number code.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="phoneNumber">Phone number.</param>
        /// <returns>The task.</returns>
        public async Task SendSmsChangePhoneNumberCodeAsync(BastionUser user, string phoneNumber)
        {
            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            var code = await userManager.GenerateChangePhoneNumberTokenAsync(user, phoneNumber);

            await smsSender.SendSmsAsync(phoneNumber, messagesService, MessageConstants.TwoFactorCode, profile.GetLanguageIdOrDefault(), new
            {
                Code = code
            });
        } 
        #endregion

        /// <summary>
        /// Send email for device confirmation.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="device">Device.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailDeviceConfirmationAsync(BastionUser user, BastionUserDevice device)
        {
            var code = await userManager.GenerateUserTokenAsync(user, "Default", "DeviceConfirmation");
            var codeBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(code));

            var profile = await repository.GetEntity<UserProfile>(x => x.UserId == user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.DeviceConfirm, profile.GetLanguageIdOrDefault(), new
            {
                UserId = user.Id,
                Code = codeBase64,
                UserName = profile.ToName(),

                device.City,
                device.Country,
                device.Browser,
                DeviceId = device.Id,
                IpAddress = device.Ip,
            });
        }
    }
}